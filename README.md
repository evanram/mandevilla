Modified version of https://github.com/baskerville/bubblegum/

Install with [vim-plug](https://github.com/junegunn/vim-plug): `Plug 'evanram/mandevilla'`

![Showcasing the colors...](https://raw.githubusercontent.com/evanram/bubblegum/master/Example.png)
